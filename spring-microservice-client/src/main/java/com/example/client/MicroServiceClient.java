package com.example.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MicroServiceClient implements CommandLineRunner {

	@Autowired
	protected MicroServiceInvoker microServiceInvoker;
    

	@Override
	public void run(String... arg0) throws Exception {
		long timeStart = new Date().getTime();
		long currentTime;
		while(true) {
			Thread.sleep(200);
			currentTime = new Date().getTime();
			System.out.println("********** Trying to invoke at: "+((currentTime - timeStart) / 1000)
					+".s ****************");
			microServiceInvoker.invokeMicroService(Integer.parseInt(arg0[0]));
			System.out.println("Invoked...");
		}
	}
}
