package com.example.client.hystrix;

import com.example.client.feign.HystrixMicroServiceFeignClient;
import com.example.client.stubs.ClientPersonsTO;
import org.springframework.stereotype.Component;

/**
 * Created by tomask79 on 14.11.16.
 */
@Component
public class MicroServiceHystrixFallback implements HystrixMicroServiceFeignClient{
    @Override
    public ClientPersonsTO getPersonsWithHystrix() {
        System.out.println("Waiting for circuit breaker to close!");
        return new ClientPersonsTO();
    }
}
