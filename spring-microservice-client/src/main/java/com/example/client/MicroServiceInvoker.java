package com.example.client;

import com.example.client.feign.HystrixMicroServiceFeignClient;
import com.example.client.feign.JacksonMicroServiceFeignClient;
import com.example.client.feign.SimpleMicroServiceFeignClient;
import com.example.client.stubs.ClientPersonTO;
import com.example.client.stubs.ClientPersonsTO;
import com.netflix.hystrix.HystrixCommand;
import feign.hystrix.HystrixFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.stereotype.Component;

/**
 * Created by tomask79 on 06.11.16.
 */
@Component
public class MicroServiceInvoker {

    @Autowired
    private SimpleMicroServiceFeignClient simpleMicroServiceFeignClient;

    @Autowired
    private JacksonMicroServiceFeignClient jacksonMicroServiceFeignClient;

    @Autowired
    private HystrixMicroServiceFeignClient hystrixMicroServiceFeignClient;

    public void invokeMicroService(int serviceType) {
        switch (serviceType) {
            case 0: {
                final String invokeResult = simpleMicroServiceFeignClient.
                        invokePersonsMicroService();
                System.out.println("*************************************************************");
                System.out.println("Simple MicroService Feign invocation result :"+invokeResult);
                System.out.println("*************************************************************");
                break;
            }
            case 1: {
                final ClientPersonsTO clientPersonsTO = jacksonMicroServiceFeignClient.
                        invokePersonsMicroService();
                System.out.println("*************************************************************");
                for (ClientPersonTO client: clientPersonsTO.getPersons()) {
                    System.out.println("Person name: "+client.getName());
                    System.out.println("Person surname: "+client.getSurname());
                    System.out.println("Person department: "+client.getDepartment());
                }
                System.out.println("*************************************************************");
                break;
            }
            case 2: {
                for (ClientPersonTO client: hystrixMicroServiceFeignClient.
                        getPersonsWithHystrix().getPersons()) {
                    System.out.println("Person name: "+client.getName());
                    System.out.println("Person surname: "+client.getSurname());
                    System.out.println("Person department: "+client.getDepartment());
                }
                break;
            }
        }
    }
}
