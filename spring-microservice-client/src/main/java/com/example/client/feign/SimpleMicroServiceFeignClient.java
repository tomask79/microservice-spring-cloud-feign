package com.example.client.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by tomask79 on 13.11.16.
 */
@Component
@FeignClient("personsService")
public interface SimpleMicroServiceFeignClient {
    @RequestMapping(method = RequestMethod.GET, value = "/persons")
    String invokePersonsMicroService();
}
