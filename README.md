# Writing MicroServices [part 3] #

## Using Netlix Feign as MicroServices invoker ##

In this post about MicroServices let's focus on the invocation of them. In the last two posts ([Part 1](https://bitbucket.org/tomask79/microservices-spring-cloud), [Part 2](https://bitbucket.org/tomask79/microservice-spring-cloud-circuit-breaker)) I showed how to invoke MicroService with [RestTemplate](Link http://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/client/RestTemplate.html) with help of [Ribbon](https://spring.io/guides/gs/client-side-load-balancing/) giving me concrete URL of the MicroService instance from the [eureka registry](https://spring.io/blog/2015/01/20/microservice-registration-and-discovery-with-spring-cloud-and-netflix-s-eureka). For example:

```
@Component
public class MicroServiceClient implements CommandLineRunner {

    @Autowired
    private LoadBalancerClient loadBalancer;

    @Override
    public void run(String... arg0) throws Exception {
        final RestTemplate restTemplate = new RestTemplate();

        final ServiceInstance serviceInstance = loadBalancer.choose("personsService");
        if (serviceInstance != null) {
            System.out.println("Invoking instance at URL: "+serviceInstance.getUri());
            System.out.println(
                restTemplate.getForObject(serviceInstance.getUri()+"/persons", String.class));
        } else {
            System.out.println("Didn't find any running instance of personsService at DiscoveryServer!");
        }
    }
}
```
Let's be honest, there is too much boilerplate code in this which is going to be repeated very often in the large system. This is the moment when [Feign](https://bushkarl.gitbooks.io/spring-cloud/content/spring_cloud_netflix/declarative_rest_client_feign.html) comes in:

Feign gives you following benefits:

* MS invocation code is created **at runtime based on the annotations**.
* You don't need to mess with any load balancers to call your MicroService.
* Your MicroService invocation system is much more maintainable. 

Previous code with using Feign is going to look like:

First, just Feign interface:

```
@Component
@FeignClient("personsService")
public interface SimpleMicroServiceFeignClient {
    @RequestMapping(method = RequestMethod.GET, value = "/persons")
    String invokePersonsMicroService();
}
```

Now invocation of the MicroService endpoint will look like:

```
@Autowired
private SimpleMicroServiceFeignClient simpleMicroServiceFeignClient;
.
.
.
final String invokeResult = simpleMicroServiceFeignClient.
                        invokePersonsMicroService();
```
Underneath there is still used Ribbon to find instance of running MicroService like before.

## Using Jackson when calling MicroService ##

Previous MicroService invocation call returned raw JSON. Well most of the time you want Java POJO. So let's create another Feign client declaration calling the same MicroService:

```
@Component
@FeignClient("personsService")
public interface JacksonMicroServiceFeignClient {
    @RequestMapping(method = RequestMethod.GET, value = "/persons")
    ClientPersonsTO invokePersonsMicroService();
}
```

where MicroService endpoint looks like:

```
@RestController
public class PersonsController {
		
    @RequestMapping("/persons")
    public Persons getPersonById() {
    	final Persons persons = new Persons();
    	final Person person = new Person();
    	person.setName("Tomas");
    	person.setSurname("Kloucek");
    	person.setDepartment("Programmer");
    	persons.getPersons().add(person);
    	
    	return persons;
    }
}
```
Notice that MicroService returns result under **Persons** object, but client gives result to the caller in the **ClientPersonsTO** object. Well, **one of the anti patterns in MicroServices writing is to share objects between client and MicroService. MicroService should always communicate with the callers over the document API like XML, JSON, SOAP or other programming language independent protocols**. And it should be entirely on the client how to convert the result...Well, why not? MS endpoint can be for example in Java where client on the other hand could be in .NET...To finish this part, invocation via JacksonMicroServiceFeignClient is going to be again just:


```
@Autowired
private JacksonMicroServiceFeignClient jacksonMicroServiceFeignClient;
.
.
final ClientPersonsTO clientPersonsTO = jacksonMicroServiceFeignClient.
                        invokePersonsMicroService();
```

## Combining Feign with Hystrix ##

In [Part 2](https://bitbucket.org/tomask79/microservice-spring-cloud-circuit-breaker) you could see how useful Hystrix is. Of course you can combine it with Feign as well. All you need to do is:

*Include Hystrix on the classpath with maven dependency:*
```
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-hystrix</artifactId>
</dependency>
```
Since now, Feign will wrap every MS call with Hystrix. You can disable it with setting of: **feign.hystrix.enabled=false** application property. Your Feign client interface methods can also return HystrixCommand to allow the caller to use reactive pattern with Observable Java. So let's create Feign Hystrix client:


```
@Component
@FeignClient(value = "personsService", fallback = MicroServiceHystrixFallback.class)
public interface HystrixMicroServiceFeignClient {
    @RequestMapping(method = RequestMethod.GET, value = "/persons")
    ClientPersonsTO getPersonsWithHystrix();
}
```

I defined here fallback if circuit-breaker will be opened. Btw when returning HystrixCommand then fallback is not supported yet. Now, to configure Hystrix...Feign Hystrix uses [archaius](https://github.com/Netflix/archaius/wiki/Users-Guide) which uses method names as command keys, so the configuration in the application.properties will be:


```
server.port=8888
eureka.client.serviceUrl.defaultZone=http://localhost:9761/eureka
hystrix.command.getPersonsWithHystrix.fallback.enabled=true
hystrix.command.getPersonsWithHystrix.metrics.rollingStats.timeInMilliseconds=35000
hystrix.command.getPersonsWithHystrix.circuitBreaker.sleepWindowInMilliseconds=5000
hystrix.command.getPersonsWithHystrix.circuitBreaker.requestVolumeThreshold=6
hystrix.command.getPersonsWithHystrix.circuitBreaker.errorThresholdPercentage=100
hystrix.command.getPersonsWithHystrix.execution.isolation.strategy=THREAD
```

## Testing the demo ##

Run the discovery server (Eureka)

```
mvn clean install (in the spring-microservice-registry directory with pom.xml)
java -jar target/demo-0.0.1-SNAPSHOT.war
```

Run the MicroService

```
mvn clean install (in the spring-microservice-service directory with pom.xml)
java -jar target/demo-0.0.1-SNAPSHOT.war
verify with http://localhost:9761 that MicroService is registered.
```

Testing all the three clients

```
mvn clean install (in the spring-microservice-client directory with pom.xml)
java -jar target/demo-0.0.1-SNAPSHOT.war [0-2]
```

Arguments [0-2]

```
0 - SimpleFeignClient, printing the result in raw JSON
1 - JacksonFeignClient, printing the result from Java POJO.
2 - HystrixFeignClient, printing the result from Java POJO
```

### Testing the Hystrix Feign client (argument 2) ###

Previous hystrix setting in the application.properties says: **Every 35 seconds, collect statistics from 6 requests and if they're all failed then OPEN the circuit. After that, try every five seconds if MS is alive again. If yes then close the circuit again**. 

So if you start all of the components then you should see at client **with argument 2**:
```
********** Trying to invoke at: 20.s ****************
Person name: Tomas
Person surname: Kloucek
Person department: Programmer
Invoked...
.
.
.
```
Now after you kill the MS, the output should be repeated **35 seconds** like:

```
********** Trying to invoke at: 25.s ****************
Waiting for circuit breaker to close!
Invoked...
.
.
.
```
After 35 seconds passed, outputting should go faster with every **5 seconds longer delay** testing the MS whether it's alive again...It's noticeable, test it.

# Summary #

Spring Cloud integration with Netflix Feign is very useful when you don't need special logic of invoking the concrete instances of MS, which is true most of the time so Feign is very handy stuff. You can use it also to access any REST endpoint also outside of eureka server...Just check 


```
https://github.com/OpenFeign/feign
```

Feign FTW!..:)

regards

Tomas